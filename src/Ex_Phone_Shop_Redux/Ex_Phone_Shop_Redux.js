import React, { Component } from 'react'
import { connect } from 'react-redux'

class Ex_Phone_Shop_Redux extends Component {
  render() {
    let {tenSP, hinhAnh, manHinh, heDieuHanh, cameraTruoc, cameraSau, rom, ram} = this.props.detail;

    return (
      <div className='container'>
        <h2>Ex_Phone_Shop_Redux</h2>
        <div className="listPhone row">
            {this.props.listPhone.map((item, index) => {
                return (
                    <div className='col-4' key={index}>
                        <div className="card p-4 border-primary">
                            <img src={item.hinhAnh} alt="" style={{width: '100%'}}/>
                            <div className="card-body">
                                <h4 className='card-title'>{item.tenSP}</h4>
                                <p className="card-text">{item.giaBan}</p>
                                <button onClick={() => {this.props.handleXemChiTiet(item)}} className='btn btn-primary'>Xem chi tiết</button>
                            </div>
                        </div>
                    </div>
                )
            })}
        </div>

        <div className="container">
            <div className='detailPhone row border border-primary rounded py-4 my-4'>
              <div className="col-6">
                  <h3>{tenSP}</h3>
                  <img src={hinhAnh} alt="" style={{width:'80%'}}/>
              </div>
              <div className="col-6">
                  <table className='table text-left'>
                      <thead>
                          <tr>
                              <td><b>Thông số kỹ thuật</b></td>
                          </tr>
                          <tr>
                              <td>Màn hình</td>
                              <td>{manHinh}</td>
                          </tr>
                          <tr>
                              <td>Hệ điều hành</td>
                              <td>{heDieuHanh}</td>
                          </tr>
                          <tr>
                              <td>Camera trước</td>
                              <td>{cameraTruoc}</td>
                          </tr>
                          <tr>
                              <td>Camera sau</td>
                              <td>{cameraSau}</td>
                          </tr>
                          <tr>
                              <td>RAM</td>
                              <td>{ram}</td>
                          </tr>
                          <tr>
                              <td>ROM</td>
                              <td>{rom}</td>
                          </tr>
                      </thead>
                  </table>
              </div>
            </div>
        </div>
      </div>
    )
  }
}

let mapStateToProps = (state) => {
    return {
        listPhone: state.phoneReducer.listPhone,
        detail: state.phoneReducer.detail,
    }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handleXemChiTiet: (selectedPhone) => {
      dispatch({
        type: 'XEM_CHI_TIET',
        payload: selectedPhone,
      })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Ex_Phone_Shop_Redux);
