import { combineReducers } from "redux";
import { phoneReducer } from "./phoneReducer";

export const rootReducer_Ex_Phone_Shop_Redux = combineReducers(
    {
        phoneReducer: phoneReducer,
    }
);