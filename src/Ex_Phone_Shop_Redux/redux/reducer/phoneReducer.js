import { data_phone } from "../../data_phone";

let initialValue = {
    listPhone: data_phone,
    detail: data_phone[0],
}

export const phoneReducer = (state=initialValue, action) => {
    switch (action.type) {
        case "XEM_CHI_TIET": {
            let selectedPhone = action.payload;
            return {...state, detail: selectedPhone}
        }
        default:
            return state;
    }
}