import logo from './logo.svg';
import './App.css';
import Ex_Phone_Shop_Redux from './Ex_Phone_Shop_Redux/Ex_Phone_Shop_Redux';

function App() {
  return (
    <div className="App">
      <Ex_Phone_Shop_Redux/>
    </div>
  );
}

export default App;
